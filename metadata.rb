name             'chef-experiment'
maintainer       'adaptation.io'
maintainer_email 'paul@adaptation.io'
license          'MIT'
description      'Installs/Configures chef-experiment'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'

depends 'nodejs'
